FROM ruby:2.4.2

ENV DEBIAN_FRONTEND noninteractive
ENV TERM xterm

ADD bashrc /root/.bashrc

RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" >> /etc/apt/sources.list.d/pgdg.list && \
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN apt-get -qqy update && \
    apt-get install -qqy --no-install-recommends \
      ca-certificates                            \
      curl                                       \
      dnsutils                                   \
      git                                        \
      jq                                         \
      libgsf-1-dev                               \
      lsb-release                                \
      lsof                                       \
      net-tools                                  \
      netcat                                     \
      procps                                     \
      traceroute                                 \
      unzip                                      \
      vim                                        \
      wget                                       \
      postgresql-client-9.6                      \
    && curl -s https://raw.githubusercontent.com/h2non/bimg/master/preinstall.sh | bash - \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs

RUN npm i -g yarn

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN bundle config --global frozen 0

COPY Gemfile /usr/src/app/
COPY Gemfile.lock /usr/src/app/

RUN bundle install

COPY . /usr/src/app
ADD docker-entrypoint.sh /usr/src/app/docker-entrypoint.sh
RUN chmod +x /usr/src/app/docker-entrypoint.sh

CMD ["/usr/src/app/docker-entrypoint.sh"]
