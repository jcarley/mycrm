class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers, id: :uuid do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :username, null: false

      t.timestamps
    end
    add_index :customers, :email, unique: true
    add_index :customers, :username, unique: true
  end
end
