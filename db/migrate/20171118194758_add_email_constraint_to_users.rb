class AddEmailConstraintToUsers < ActiveRecord::Migration[5.1]
  def up
    execute %{
      ALTER TABLE
        users
      ADD CONSTRAINT
        email_must_look_like_an_email
      UNIQUE (email)
    }
  end

  def down
    execute %{
      ALTER TABLE
        users
      DROP CONSTRAINT
        email_must_look_like_an_email
    }
  end
end
