class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events, id: :uuid do |t|
      t.string :name
      t.datetime :start_dt
      t.datetime :end_dt

      t.timestamps
    end
  end
end
