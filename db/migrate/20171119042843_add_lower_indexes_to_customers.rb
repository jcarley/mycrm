class AddLowerIndexesToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_index :customers, "lower(name) varchar_pattern_ops"
    add_index :customers, "lower(email)"
  end
end
