class AddCompanyToCustomer < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :company, :string, limit: 128
  end
end
