
// import shop from '../../api/shop'
import customerApi from '@/api/customers';
import * as types from '@/store/mutation-types';

// initial state
const state = {
  query: {
    keywords: '',
    page: 1
  },
  customers: [],
  currentCustomer: {}
}

// getters
const getters = {
  customers: state => state.customers,
  currentCustomer: state => state.currentCustomer,
  currentPage: state => state.query.page,
  searchKeywords: state => state.query.keywords
}

// actions
const actions = {

  search({ commit, state }, query) {

    let searchQuery = { ...state.query, ...query };

    customerApi.search(searchQuery).then(({data}) => {
      commit(types.CUSTOMER_SEARCH_SUCCESS, { query: query, results: data });
    }).catch((err) => {
      commit(types.CUSTOMER_SEARCH_FAILURE, { error: err });
    });

  },

  showCustomer({ commit, state }, query) {
    console.log('showCustomer', query);
    customerApi.show(query).then(({data}) => {
      commit(types.CUSTOMER_SHOW_SUCCESS, { currentCustomer: data });
    }).catch((err) => {
      commit(types.CUSTOMER_SHOW_FAILURE, { error: err });
    });
  }
}

// mutations
const mutations = {
  [types.CUSTOMER_SEARCH_SUCCESS] (state, { query, results }) {
    state.query.keywords = query.keywords;
    state.query.page = query.page;
    state.customers = results;
  },

  [types.CUSTOMER_SEARCH_FAILURE] (state, { error }) {
  },

  [types.CUSTOMER_SHOW_SUCCESS] (state, { currentCustomer }) {
    state.currentCustomer = currentCustomer;
  },

  [types.CUSTOMER_SHOW_FAILURE] (state, { error }) {
  }

}

export default {
  state,
  getters,
  actions,
  mutations
}

