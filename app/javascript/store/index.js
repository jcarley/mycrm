// import * as actions from './actions'
// import * as getters from './getters'
import * as types from './mutation-types';

import customers from './modules/customers';

const state = {
};

const actions = {
};

const mutations = {
};

export default {
  state,
  mutations,
  actions,
  modules: {
    customers
  },
};

