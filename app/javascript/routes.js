
import CustomerPage from '@/customers/customers-page.vue';
import CustomerList from '@/customers/customer-list.vue';
import CustomerDetail from '@/customers/customer-detail.vue';

export default [
  {
    path: '/customers',
    component: CustomerPage,
    redirect: '/customers/list',
    children: [
      {
        path: 'list',
        component: CustomerList
      },
      {
        path: 'detail/:id',
        component: CustomerDetail
      }
    ]
  }
]
