
import axios from 'axios';

export default {
  async search(query) {
    let json = await axios.get('http://localhost:3000/api/customers/search', {
      params: query
    });
    return json
  },

  async show(query) {
    let json = await axios.get(`http://localhost:3000/api/customers/${query.id}`)
    return json
  }

};
