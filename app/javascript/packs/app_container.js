/* eslint no-console: 0 */
import Vue from 'vue/dist/vue.esm';
import Vuex from 'vuex';
import VueRouter from 'vue-router';

import storeObject from '@/store';
import routes from '@/routes';
import App from '@/app.vue';

Vue.use(Vuex);
Vue.use(VueRouter);

const store = new Vuex.Store(
  storeObject
);

const router = new VueRouter({
  routes
});

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    el: '#app_container',
    store,
    router,
    components: { App }
  });
});
