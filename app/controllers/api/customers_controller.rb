class Api::CustomersController < Api::ApiController
  PAGE_SIZE = 50

  def search
    @page = (params[:page] || 0).to_i

    if params[:keywords].present?
      @keywords = params[:keywords]
      customer_search_term = CustomerSearchTerm.new(@keywords)
      @customers = Customer.where(customer_search_term.where_clause,
                                  customer_search_term.where_args).
                                  order(customer_search_term.order).
                                  offset(PAGE_SIZE * @page).limit(PAGE_SIZE)
    else
      @customers = []
    end

    render json: @customers, status: :ok
  end

  def show
    @customer = Customer.find(params[:id])
    render json: @customer, status: :ok
  rescue ActiveRecord::RecordNotFound
    render_error_message("Unable to find customer with id #{params[:id]}", :not_found)
  end

end
