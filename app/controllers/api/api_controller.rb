class Api::ApiController < ApplicationController

  def render_error_message(msg, status)
    render json: {error: msg, code: Rack::Utils.status_code(status)}, status: status
  end

end
