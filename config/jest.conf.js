const path = require('path')

module.exports = {
  rootDir: path.resolve(__dirname, '../'),
  roots: ["<rootDir>/app/javascript/"],
  moduleFileExtensions: [
    'js',
    'json',
    'vue'
  ],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/app/javascript/$1'
  },
  testRegex: "((\\.|/)(test|spec))\\.js$",
  transform: {
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest'
  },
  testPathIgnorePatterns: [
    '<rootDir>/test/e2e'
  ],
  snapshotSerializers: ['<rootDir>/node_modules/jest-serializer-vue'],
  setupFiles: ['<rootDir>/config/testsetup'],
  mapCoverage: true,
  coverageDirectory: '<rootDir>/test/unit/coverage',
  collectCoverageFrom: [
    'app/javascript/**/*.{js,vue}',
    '!app/javascript/router/index.js',
    '!app/javascript/packs/**/*.js',
    '!**/node_modules/**'
  ]
}
