Rails.application.routes.draw do
  root to: "dashboard#index"
  devise_for :users

  resources :customers, only: [ :index ]

  defaults format: :json do
    namespace :api do
      resources :customers, only: [ :show ] do
        collection do
          get 'search'
        end
      end
    end
  end

end
