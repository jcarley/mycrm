const merge = require('webpack-merge')
const environment = require('./environment')
const baseConfig = require('./base')

module.exports = merge(environment.toWebpackConfig(), baseConfig)
