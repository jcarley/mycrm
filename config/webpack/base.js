const resolve = require('path').resolve;

module.exports = {
  resolve: {
    alias: {
      '@': resolve(__dirname, '..', '..', 'app/javascript'),
    }
  }
}
