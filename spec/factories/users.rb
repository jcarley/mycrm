FactoryBot.define do

  factory :user do
    sequence(:email) { |i| "#{Faker::Internet.user_name}#{i}@#{Faker::Internet.domain_name}" }
    password Faker::Internet.password(10, 20)
    encrypted_password Faker::Internet.password(10, 20)
  end

end
