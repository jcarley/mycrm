FactoryBot.define do

  factory :customer do
    name     "#{Faker::Name.first_name} #{Faker::Name.last_name}"
    sequence(:username) { |i| "#{Faker::Internet.user_name}#{i}" }
    sequence(:email) { |i| "#{Faker::Internet.user_name}#{i}@#{Faker::Internet.domain_name}" }
    company  "#{Faker::Company.name}"
  end

end
