
def body_as_json
  json_str_to_hash(response.body)
end

def json_str_to_hash(str)
  value = JSON.parse(str)

  return value if value.is_a? Array
  return value.with_indifferent_access if value.is_a? Hash
  return value
end

