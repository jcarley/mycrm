require 'rails_helper'

RSpec.describe "customers api", type: :request do

  let(:user) { FactoryBot.create(:user) }
  let(:customer) { FactoryBot.create(:customer) }

  describe "search" do

  end

  describe "show" do
    it 'returns a specific customers information' do
      sign_in user

      get "/api/customers/#{customer.id}"

      expected_customer = {
        id: customer.id,
        name: customer.name,
        username: customer.username,
        email: customer.email,
        company: customer.company
      }

      expect(response.content_type).to eq("application/json")
      expect(response).to have_http_status(:ok)
      expect(body_as_json).to match(hash_including(expected_customer))
    end

    it "has a error message when customer is not found" do
      sign_in user

      # this will always fail because we uuid for the primary key
      get "/api/customers/1"

      expect(response.content_type).to eq("application/json")
      expect(response).to have_http_status(:not_found)
      expect(body_as_json).to match(hash_including(:error, :code))
    end

  end

end
