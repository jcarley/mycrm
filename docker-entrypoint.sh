#!/usr/bin/env bash

log() {
  echo -e "## $(date +%FT%H:%M:%S%z) ${0} ${@}"
}

log "Initializing container..."

log "Running bundle install ..."
bundle install --jobs 4 && log "Finished installing new gems."


log "Environment:" && env | sort

log "RAILS_ENV=${RAILS_ENV}"

cd /usr/src/app

log "Clearing tmp ..."
bundle exec rails tmp:clear
rm /usr/src/app/tmp/pids/server.pid

log "Starting foreman ..."
bundle exec foreman start

